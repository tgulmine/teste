import React from 'react';
import { mount } from 'enzyme';
import App from './App';
import { BrowserRouter } from 'react-router-dom';

describe('App', () => {
    it('renders without crashing', () => {
        const wrapper = mount(
            <BrowserRouter>
                <App />
            </BrowserRouter>
        );

        expect(wrapper.find(App)).toBeDefined();
    });
});
