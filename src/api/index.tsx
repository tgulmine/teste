import axios from 'axios';

export const pokemonAPI = axios.create({
    baseURL: 'https://pokeapi.co/api/v2/pokemon',
});

export const speciesAPI = axios.create({
    baseURL: 'https://pokeapi.co/api/v2/pokemon-species/?limit=0',
});

export const typesAPI = axios.create({
    baseURL: 'https://pokeapi.co/api/v2/type/',
});
