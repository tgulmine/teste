import React from 'react';
import { mount } from 'enzyme';
import HomePage from './HomePage';
import { BrowserRouter } from 'react-router-dom';

describe('HomePage', () => {
    it('renders without crashing', () => {
        const wrapper = mount(
            <BrowserRouter>
                <HomePage />
            </BrowserRouter>
        );
        expect(wrapper.find(HomePage)).toBeDefined();
    });
});
