import React from 'react';
import './HomePage.scss';
import { Link } from 'react-router-dom';
import pokemonLogo from '../../assets/images/pokemonLogo.png';

const HomePage = () => {
    return (
        <div className="home">
            <img className="home-logo" src={pokemonLogo} alt="" />
            <Link to="/map">
                <button className="home-button typo typo--title">START</button>
            </Link>
        </div>
    );
};

export default HomePage;
