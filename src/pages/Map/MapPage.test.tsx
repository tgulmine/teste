import React from 'react';
import { mount } from 'enzyme';
import MapPage from './MapPage';

describe('MapPage', () => {
    it('renders without crashing', () => {
        const wrapper = mount(<MapPage />);
        expect(wrapper.find(MapPage)).toBeDefined();
    });
    it('modal not visible on first load', () => {
        const wrapper = mount(<MapPage />);
        expect(wrapper.find('.modal')).toHaveLength(0);
    });
});
