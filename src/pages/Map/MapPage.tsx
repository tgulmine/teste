import React from 'react';
import './MapPage.scss';
import { Sidebar } from '../../components/Sidebar';
import { Ash } from '../../components/Ash';
import { Modal } from '../../components/Modal';
import { ModalData } from '../../components/ModalData';
import { ModalImg } from '../../components/ModalImg';
import { MapProvider, useMapContext } from '../../context/mapContext';
import { PokemonListProvider } from '../../context/pokemonListContext';
import { ModalButton } from '../../components/ModalButton';

const MapPage = () => {
    const { isModalOpen, setModalOpen } = useMapContext();

    return (
        <div className="map">
            <Sidebar />
            <Ash />
            <Modal isOpen={isModalOpen} toggle={setModalOpen}>
                <ModalImg />
                <ModalData />
                <ModalButton />
            </Modal>
        </div>
    );
};

const MapPageWithProvider = () => {
    return (
        <MapProvider>
            <PokemonListProvider>
                <MapPage />
            </PokemonListProvider>
        </MapProvider>
    );
};

export default MapPageWithProvider;
