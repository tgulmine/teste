import * as React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { MapPage } from './pages/Map';
import { HomePage } from './pages/Home';

const App = () => (
    <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/map" component={MapPage} />
        <Redirect to="/" />
    </Switch>
);

export default App;
