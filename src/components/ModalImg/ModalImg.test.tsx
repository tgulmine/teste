import React from 'react';
import { mount } from 'enzyme';
import { ModalImg } from '.';

describe('ModalImg', () => {
    it('renders without crashing', () => {
        const wrapper = mount(<ModalImg />);
        expect(wrapper.find(ModalImg)).toBeDefined();
    });
    it('has img', () => {
        const wrapper = mount(<ModalImg />);
        expect(wrapper.find('img').length).toEqual(1);
    });
});
