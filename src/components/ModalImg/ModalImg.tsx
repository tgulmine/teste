import React from 'react';
import './ModalImg.scss';
import { usePokemonList } from '../../context/pokemonListContext';
import { useMapContext, MODAL_TYPES } from '../../context/mapContext';
import addImage from '../../assets/images/addImage.png';

const ModalImg = () => {
    const { pokemonList, pokemonTemp } = usePokemonList();
    const { modalType, sidebarIndex } = useMapContext();
    var image;

    if (modalType === MODAL_TYPES.NEW) image = addImage;
    else if (modalType === MODAL_TYPES.CAPTURE) image = pokemonTemp.sprite;
    else image = pokemonList[sidebarIndex].sprite;

    if (!image) image = addImage;

    return (
        <div className="modalImg-container">
            <img src={image} className="modalImg-image pixel" alt="" />
        </div>
    );
};

export default ModalImg;
