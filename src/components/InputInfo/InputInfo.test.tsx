import React from 'react';
import { mount } from 'enzyme';
import { InputInfo } from '.';

describe('InputInfo', () => {
    it('renders without crashing', () => {
        const name: string = 'name';
        const setName: React.Dispatch<any> = () => null;
        const wrapper = mount(
            <InputInfo
                name="Nome"
                type="text"
                info={name}
                setInfo={setName}
                placeholder="Nome"
                icon=""
                tag=""
            />
        );
        expect(wrapper.find(InputInfo)).toBeDefined();
    });
    it('has exactly one input', () => {
        const name: string = 'name';
        const setName: React.Dispatch<any> = () => null;
        const wrapper = mount(
            <InputInfo
                name="Nome"
                type="text"
                info={name}
                setInfo={setName}
                placeholder="Nome"
                icon=""
                tag=""
            />
        );
        expect(wrapper.find('input').length).toEqual(1);
    });
});
