import React, { Dispatch, SetStateAction } from 'react';
import './InputInfo.scss';
import chevronUpIcon from '../../assets/images/chevronUpIcon.png';
import chevronDownIcon from '../../assets/images/chevronDownIcon.png';

interface IInputInfo {
    name: string;
    type: string;
    info: any;
    setInfo: Dispatch<SetStateAction<any>>;
    placeholder: string;
    icon: string;
    tag: string;
}

const InputInfo = ({
    name,
    type,
    info,
    setInfo,
    placeholder,
    icon,
    tag,
}: IInputInfo) => {
    if (name && type === 'text' && icon === '' && tag === '') {
        return (
            <div className="createPokemon-info">
                <div className="createPokemon-info__title typo typo--label">
                    {name}
                </div>
                <input
                    className="createPokemon-input createPokemon-input__text typo typo--subtitle"
                    id={`inputNew${name}`}
                    value={info}
                    onChange={e => {
                        setInfo(e.target.value);
                    }}
                    type="text"
                    placeholder={placeholder}
                    required
                />
            </div>
        );
    } else if (type === 'number' && icon === '') {
        return (
            <div className="createPokemon-info">
                <div className="createPokemon-info__title typo typo--label">
                    {name}
                </div>
                <div className="createPokemon-input createPokemon-input__number">
                    <input
                        className="typo typo--subtitle"
                        id={`inputNew${name}`}
                        value={info !== 0 ? info : ''}
                        onChange={e => {
                            setInfo(e.target.valueAsNumber);
                        }}
                        type="number"
                        placeholder={placeholder}
                        required
                    />
                    <div className="createPokemon-input__buttons">
                        <button
                            type="button"
                            className="createPokemon-input__buttons-up"
                            onClick={() => setInfo(info + 1)}
                        >
                            <img
                                className="createPokemon-input__buttons-img"
                                src={chevronUpIcon}
                                alt=""
                            />
                        </button>
                        <button
                            type="button"
                            className="createPokemon-input__buttons-down"
                            onClick={() => setInfo(info !== 0 ? info - 1 : 0)}
                        >
                            <img
                                className="createPokemon-input__buttons-img"
                                src={chevronDownIcon}
                                alt=""
                            />
                        </button>
                    </div>
                </div>
                {tag && (
                    <div className="createPokemon-input__tag typo typo--subtitle">
                        {tag}
                    </div>
                )}
            </div>
        );
    } else if (name === '') {
        return (
            <input
                className="createPokemon-input createPokemon-input__text typo typo--subtitle"
                id={`inputNew${placeholder}`}
                value={info}
                onChange={e => {
                    setInfo(e.target.value);
                }}
                type="text"
                placeholder={placeholder}
                required
            />
        );
    } else if (icon) {
        return (
            <div className="createPokemon-info">
                <div className="createPokemon-info__withIcon">
                    <div className="createPokemon-info__icon">
                        <img
                            className="createPokemon-info__icon-img"
                            src={icon}
                            alt=""
                        />
                    </div>
                    <div className="typo typo--label">{name}</div>
                </div>
                <div className="createPokemon-input createPokemon-input__number">
                    <input
                        className="typo typo--subtitle"
                        id={`inputNew${name}`}
                        value={info !== 0 ? info : ''}
                        onChange={e => {
                            setInfo(e.target.valueAsNumber);
                        }}
                        type="number"
                        placeholder={placeholder}
                        required
                    />
                    <div className="createPokemon-input__buttons">
                        <button
                            type="button"
                            className="createPokemon-input__buttons-up"
                            onClick={() => setInfo(info + 1)}
                        >
                            <img
                                className="createPokemon-input__buttons-img"
                                src={chevronUpIcon}
                                alt=""
                            />
                        </button>
                        <button
                            type="button"
                            className="createPokemon-input__buttons-down"
                            onClick={() => setInfo(info !== 0 ? info - 1 : 0)}
                        >
                            <img
                                className="createPokemon-input__buttons-img"
                                src={chevronDownIcon}
                                alt=""
                            />
                        </button>
                    </div>
                </div>
            </div>
        );
    }
    return null;
};

export default InputInfo;
