import React from 'react';
import { mount } from 'enzyme';
import { Modal } from '.';
import { ModalImg } from '../ModalImg';

describe('Modal', () => {
    it('renders without crashing', () => {
        const isModalOpen: boolean = true;
        const setModalOpen: React.Dispatch<any> = () => null;
        const wrapper = mount(
            <Modal isOpen={isModalOpen} toggle={setModalOpen}>
                <ModalImg />
            </Modal>
        );
        expect(wrapper.find(Modal)).toBeDefined();
    });
    it('does not render if isOpen is false', () => {
        const isModalOpen: boolean = false;
        const setModalOpen: React.Dispatch<any> = () => null;
        const wrapper = mount(
            <Modal isOpen={isModalOpen} toggle={setModalOpen}>
                <ModalImg />
            </Modal>
        );
        expect(wrapper.html()).toBeNull();
    });
});
