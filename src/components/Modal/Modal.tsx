import React, { ReactNode, Dispatch, SetStateAction } from 'react';
import './Modal.scss';
import closeIcon from '../../assets/images/closeIcon.png';

interface IModal {
    children: ReactNode;
    isOpen: boolean;
    toggle: Dispatch<SetStateAction<boolean>>;
}

const Modal = ({ children, isOpen, toggle }: IModal) => {
    if (!isOpen) {
        return null;
    }
    return (
        <div className="modal">
            <div className="modal__content">
                <button
                    className="button-close"
                    onClick={() => {
                        toggle(!isOpen);
                    }}
                >
                    <img src={closeIcon} alt="" />
                </button>
                {children}
            </div>
        </div>
    );
};

export default Modal;
