import React from 'react';
import { mount } from 'enzyme';
import { ModalData } from '.';

describe('ModalData', () => {
    it('renders without crashing', () => {
        const wrapper = mount(<ModalData />);
        expect(wrapper.find(ModalData)).toBeDefined();
    });
    it('has component PokemonInfo', () => {
        const wrapper = mount(<ModalData />);
        expect(wrapper.find('PokemonInfo').length).toEqual(1);
    });
});
