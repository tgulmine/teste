import React from 'react';
import './ModalData.scss';
import { useMapContext, MODAL_TYPES } from '../../context/mapContext';
import { PokemonInfo } from '../PokemonInfo';
import { CreatePokemon } from '../CreatePokemon';

const ModalData = () => {
    const { modalType } = useMapContext();

    if (modalType === MODAL_TYPES.NEW) {
        return (
            <div className="modalData">
                <CreatePokemon />
            </div>
        );
    }

    return (
        <div className="modalData">
            <PokemonInfo />
        </div>
    );
};

export default ModalData;
