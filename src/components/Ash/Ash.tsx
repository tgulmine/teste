import React, { useState, useEffect } from 'react';
import { pokemonAPI, speciesAPI } from '../../api';
import ashFront from '../../assets/images/ashFront.png';
import ashLeftLeg from '../../assets/images/ashLeftLeg.png';
import ashRightLeg from '../../assets/images/ashRightLeg.png';
import ashStop from '../../assets/images/ashStop.png';
import tooltipSearch from '../../assets/images/searchTooltip.png';
import tooltipError from '../../assets/images/errorTooltip.png';
import tooltipLoading from '../../assets/images/loadingTooltip.png';
import { usePokemonList, Pokemon } from '../../context/pokemonListContext';
import { useMapContext } from '../../context/mapContext';
import './Ash.scss';

interface ISprites {
    front: string;
    leftLeg: string;
    rightLeg: string;
    stop: string;
}
interface ITooltip {
    search: string;
    error: string;
    loading: string;
}

const Ash: React.FC = () => {
    const [isHovered, setIsHovered] = useState(false);
    const [canSearch, setCanSearch] = useState(true);
    const [pokemonCount, setPokemonCount] = useState(891);

    const { setPokemonTemp, isPokemonListFull } = usePokemonList();
    const { openCaptureModal } = useMapContext();

    const ashSprites: ISprites = {
        front: ashFront,
        leftLeg: ashLeftLeg,
        rightLeg: ashRightLeg,
        stop: ashStop,
    };
    const [ashSprite, setAshSprite] = useState(ashSprites.front);
    const frameTime = 333; //333

    const tooltipImgs: ITooltip = {
        search: tooltipSearch,
        error: tooltipError,
        loading: tooltipLoading,
    };
    const [tooltipImg, setTooltipImg] = useState(tooltipImgs.search);

    useEffect(() => {
        getPokemonCount();
    }, []);

    useEffect(() => {
        checkIfPokemonListIsFull();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isPokemonListFull]);

    function checkIfPokemonListIsFull() {
        if (isPokemonListFull) {
            setTooltipImg(tooltipImgs.error);
            setCanSearch(false);
        } else {
            setTooltipImg(tooltipImgs.search);
            setCanSearch(true);
        }
    }

    async function getPokemonCount() {
        try {
            const res = await speciesAPI.get('');
            setPokemonCount(res.data.count);
        } catch (err) {
            console.log(err);
        }
    }

    function getRandomNumber(min: number, max: number) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    }

    async function getData() {
        const id: number = getRandomNumber(1, pokemonCount + 1);
        try {
            const res = await pokemonAPI.get(`/${id}`);

            const types: string[] = [];
            res.data.types.forEach((type: any) => {
                types.push(type.type.name);
            });
            const abilities: string[] = [];
            res.data.abilities.forEach((ability: any) => {
                abilities.push(ability.ability.name);
            });

            const newPokemon: Pokemon = {
                sprite: res.data.sprites.front_default,
                name: res.data.name,
                height: res.data.height,
                weight: res.data.weight,
                hp: res.data.stats[0].base_stat,
                types: types,
                abilities: abilities,
                attack: res.data.stats[1].base_stat,
                defense: res.data.stats[2].base_stat,
                specialAttack: res.data.stats[3].base_stat,
                specialDefense: res.data.stats[4].base_stat,
                speed: res.data.stats[5].base_stat,
            };

            setPokemonTemp(newPokemon);
        } catch (err) {
            console.log(err);
        }
    }

    function ashAnimation() {
        for (let i = 0; i < 15; i++) {
            setTimeout(() => {
                const sprite: string =
                    i === 14
                        ? ashSprites.front
                        : i === 2 || i === 6
                        ? ashSprites.rightLeg
                        : i === 0 || i === 4
                        ? ashSprites.leftLeg
                        : ashSprites.stop;
                if (i === 14) {
                    setCanSearch(true);
                    openCaptureModal();
                    setTooltipImg(tooltipImgs.search);
                }
                setAshSprite(sprite);
            }, i * frameTime);
        }
    }

    function searchPokemon() {
        if (canSearch) {
            setCanSearch(false);
            setTooltipImg(tooltipImgs.loading);
            getData();
            ashAnimation();
        }
    }

    return (
        <button
            className={
                !isPokemonListFull ? 'ash pixel' : 'ash pixel ash-disabled'
            }
            style={{
                backgroundImage: `url('${ashSprite}')`,
            }}
            onMouseEnter={() => setIsHovered(true)}
            onMouseLeave={() => setIsHovered(false)}
            onClick={searchPokemon}
        >
            {(isHovered || !canSearch) && (
                <div
                    className="tooltip"
                    style={{
                        backgroundImage: `url('${tooltipImg}')`,
                    }}
                />
            )}
        </button>
    );
};

export default Ash;
