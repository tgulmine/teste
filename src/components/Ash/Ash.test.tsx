import React from 'react';
import { mount } from 'enzyme';
import { Ash } from '.';

describe('Ash', () => {
    it('renders without crashing', () => {
        const wrapper = mount(<Ash />);
        expect(wrapper.find(Ash)).toBeDefined();
    });
    it('ash is not disabled', () => {
        const wrapper = mount(<Ash />);
        expect(wrapper.find('.ash-disabled')).toHaveLength(0);
    });
});
