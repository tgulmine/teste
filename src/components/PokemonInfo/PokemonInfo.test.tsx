import React from 'react';
import { mount } from 'enzyme';
import { PokemonInfo } from '.';
import { ModalSection } from '../ModalSection';

describe('PokemonInfo', () => {
    it('renders without crashing', () => {
        const wrapper = mount(<PokemonInfo />);
        expect(wrapper.find(PokemonInfo)).toBeDefined();
    });
    it('input is disabled', () => {
        const wrapper = mount(<PokemonInfo />);
        expect(wrapper.find('input').length).toEqual(0);
    });
    it('renders correct number of sections', () => {
        const wrapper = mount(<PokemonInfo />);
        expect(wrapper.find(ModalSection).length).toEqual(2);
    });
});
