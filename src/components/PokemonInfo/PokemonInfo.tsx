import React, { useState } from 'react';
import './PokemonInfo.scss';
import {
    usePokemonList,
    Pokemon,
    newPokemon,
} from '../../context/pokemonListContext';
import { useMapContext, MODAL_TYPES } from '../../context/mapContext';
import attackIcon from '../../assets/images/attackIcon.png';
import defenseIcon from '../../assets/images/defenseIcon.png';
import speedIcon from '../../assets/images/speedIcon.png';
import editIcon from '../../assets/images/editIcon.png';
import checkIcon from '../../assets/images/checkIcon.png';
import closeIcon from '../../assets/images/closeIcon.png';
import { ModalSection } from '../ModalSection';

const PokemonInfo = () => {
    const [isEditModeOn, setEditModeOn] = useState<boolean>(false);
    const [newName, setNewName] = useState('');
    const { pokemonList, pokemonTemp, editPokemon } = usePokemonList();
    const { modalType, sidebarIndex } = useMapContext();

    var pokemon: Pokemon = newPokemon;

    if (modalType === MODAL_TYPES.CAPTURE) pokemon = pokemonTemp;
    else if (modalType === MODAL_TYPES.BASE)
        pokemon = pokemonList[sidebarIndex];

    function updateName() {
        pokemon.name = newName;
        editPokemon(pokemon, sidebarIndex);
        setNewName('');
        setEditModeOn(false);
    }

    function closeEditMode() {
        setNewName('');
        setEditModeOn(false);
    }

    return (
        <div className="pokemonInfo">
            {modalType === MODAL_TYPES.CAPTURE ? (
                <div className="pokemonInfo-name">
                    <div className="pokemonInfo-name__text typo typo--title">
                        {pokemon.name.toUpperCase()}
                    </div>
                </div>
            ) : isEditModeOn ? (
                <div className="pokemonInfo-edit">
                    <input
                        className="pokemonInfo-edit__input typo typo--subtitle"
                        id="inputNewName"
                        value={newName}
                        onChange={e => {
                            setNewName(e.target.value);
                        }}
                        type="name"
                        placeholder={pokemon.name}
                    />
                    <button
                        className="pokemonInfo-edit__button"
                        onClick={() => updateName()}
                    >
                        <img src={checkIcon} alt="" />
                    </button>
                    <button
                        className="pokemonInfo-edit__button"
                        onClick={() => closeEditMode()}
                    >
                        <img src={closeIcon} alt="" />
                    </button>
                </div>
            ) : (
                <div className="pokemonInfo-name pokemonInfo-name__margin">
                    <div className="pokemonInfo-name__text typo typo--title">
                        {pokemon.name.toUpperCase()}
                    </div>
                    <button
                        className="pokemonInfo-name__button"
                        style={{
                            backgroundImage: `url('${editIcon}')`,
                        }}
                        onClick={() => setEditModeOn(true)}
                    />
                </div>
            )}
            <div className="pokemonInfo-about">
                <div className="">
                    <div className="typo typo--label">Hp</div>
                    <div className="typo typo--title pokemonInfo-about__margin">
                        {pokemon.hp}/{pokemon.hp}
                    </div>
                </div>
                <div className="pokemonInfo-about__borders">
                    <div className="typo typo--label">Altura</div>
                    <div className="typo typo--title pokemonInfo-about__margin">
                        {(Math.round(pokemon.height * 100) / 1000).toFixed(1)} m
                    </div>
                </div>
                <div className="">
                    <div className="typo typo--label">Peso</div>
                    <div className="typo typo--title pokemonInfo-about__margin">
                        {(Math.round(pokemon.weight * 100) / 1000).toFixed(1)}{' '}
                        kg
                    </div>
                </div>
            </div>
            <ModalSection title="TIPO" />
            <div className="pokemonInfo-types">
                {pokemon &&
                    pokemon.types.map((type, index) => (
                        <div
                            key={index}
                            className={
                                'pokemonInfo-types__type typo typo--label type--' +
                                type
                            }
                        >
                            {type}
                        </div>
                    ))}
            </div>
            <ModalSection title="HABILIDADES" />
            <div className="pokemonInfo-abilities typo typo--label">
                {pokemon &&
                    pokemon.abilities.map((ability, index) =>
                        index < pokemon.abilities.length - 1
                            ? `${ability}, `
                            : ability
                    )}
            </div>
            {modalType === MODAL_TYPES.BASE && (
                <>
                    <ModalSection title="ESTATÍSTICAS" />
                    <div className="pokemonInfo-stats">
                        <div className="pokemonInfo-stats__icon">
                            <img
                                className="pokemonInfo-stats__icon-img"
                                src={defenseIcon}
                                alt=""
                            />
                        </div>
                        <div className="pokemonInfo-stats__name typo typo--label">
                            Defesa
                        </div>
                        <div className="pokemonInfo-stats__value typo typo--subtitle">
                            {pokemon.defense}
                        </div>
                    </div>
                    <div className="pokemonInfo-stats">
                        <div className="pokemonInfo-stats__icon">
                            <img
                                className="pokemonInfo-stats__icon-img"
                                src={attackIcon}
                                alt=""
                            />
                        </div>
                        <div className="pokemonInfo-stats__name typo typo--label">
                            Ataque
                        </div>
                        <div className="pokemonInfo-stats__value typo typo--subtitle">
                            {pokemon.attack}
                        </div>
                    </div>
                    <div className="pokemonInfo-stats">
                        <div className="pokemonInfo-stats__icon">
                            <img
                                className="pokemonInfo-stats__icon-img"
                                src={defenseIcon}
                                alt=""
                            />
                        </div>
                        <div className="pokemonInfo-stats__name typo typo--label">
                            Defesa Especial
                        </div>
                        <div className="pokemonInfo-stats__value typo typo--subtitle">
                            {pokemon.specialDefense}
                        </div>
                    </div>
                    <div className="pokemonInfo-stats">
                        <div className="pokemonInfo-stats__icon">
                            <img
                                className="pokemonInfo-stats__icon-img"
                                src={attackIcon}
                                alt=""
                            />
                        </div>
                        <div className="pokemonInfo-stats__name typo typo--label">
                            Ataque Especial
                        </div>
                        <div className="pokemonInfo-stats__value typo typo--subtitle">
                            {pokemon.specialAttack}
                        </div>
                    </div>
                    <div className="pokemonInfo-stats">
                        <div className="pokemonInfo-stats__icon">
                            <img
                                className="pokemonInfo-stats__icon-img__speed"
                                src={speedIcon}
                                alt=""
                            />
                        </div>
                        <div className="pokemonInfo-stats__name typo typo--label">
                            Velocidade
                        </div>
                        <div className="pokemonInfo-stats__value typo typo--subtitle">
                            {pokemon.speed}
                        </div>
                    </div>
                </>
            )}
            <div className={'pokemonInfo-blank__' + modalType} />
        </div>
    );
};

export default PokemonInfo;
