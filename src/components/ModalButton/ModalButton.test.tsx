import React from 'react';
import { mount } from 'enzyme';
import { ModalButton } from '.';

describe('ModalButton', () => {
    it('renders without crashing', () => {
        const wrapper = mount(<ModalButton />);
        expect(wrapper.find(ModalButton)).toBeDefined();
    });
    it('has exactly one button', () => {
        const wrapper = mount(<ModalButton />);
        expect(wrapper.find('button').length).toEqual(1);
    });
});
