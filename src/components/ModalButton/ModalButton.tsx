import React from 'react';
import './ModalButton.scss';
import { usePokemonList } from '../../context/pokemonListContext';
import { useMapContext, MODAL_TYPES } from '../../context/mapContext';
import pokeball from '../../assets/images/pokeball.png';

const ModalButton = () => {
    const { pokemonTemp, addPokemon, releasePokemon } = usePokemonList();
    const { modalType, setModalOpen, sidebarIndex } = useMapContext();

    function capture() {
        addPokemon(pokemonTemp);
        setModalOpen(false);
    }

    function liberar() {
        releasePokemon(sidebarIndex);
        setModalOpen(false);
    }

    function create() {
        addPokemon(pokemonTemp);
        setModalOpen(false);
    }

    if (modalType === MODAL_TYPES.CAPTURE) {
        return (
            <button
                className="modalButton modalButton-capture"
                style={{
                    backgroundImage: `url('${pokeball}')`,
                }}
                onClick={capture}
            />
        );
    } else if (modalType === MODAL_TYPES.BASE) {
        return (
            <button
                className="modalButton modalButton-default typo typo--title"
                onClick={liberar}
            >
                LIBERAR POKEMON
            </button>
        );
    } else {
        return (
            <button
                className="modalButton modalButton-default typo typo--title"
                onClick={create}
            >
                CRIAR POKEMON
            </button>
        );
    }
};

export default ModalButton;
