import * as React from 'react';
import './Sidebar.scss';
import { usePokemonList } from '../../context/pokemonListContext';
import plusIcon from '../../assets/images/plusIcon.png';
import addImage from '../../assets/images/addImage.png';
import { useMapContext } from '../../context/mapContext';

const Sidebar: React.FC = () => {
    const { pokemonList, isPokemonListFull } = usePokemonList();
    const { openBaseModal, openNewModal } = useMapContext();

    const iterator: number[] = [];
    for (let i = 0; i < 6; i++) {
        iterator[i] = i;
    }

    function tryOpenNewModal() {
        if (!isPokemonListFull) openNewModal();
    }

    return (
        <div className="sidebar">
            {iterator &&
                iterator.map((n, index) =>
                    pokemonList[index] ? (
                        <button
                            key={index}
                            className="sidebar-item sidebar-item__pokemon"
                            onClick={() => openBaseModal(index)}
                        >
                            <img
                                className="sidebar-item__pokemon-img"
                                src={
                                    pokemonList[index].sprite
                                        ? pokemonList[index].sprite
                                        : addImage
                                }
                                alt={pokemonList[index].name}
                            />
                        </button>
                    ) : (
                        <div
                            key={index}
                            className="sidebar-item sidebar-item__empty"
                        >
                            ?
                        </div>
                    )
                )}
            <button
                className="sidebar-item sidebar-item__new"
                onClick={() => tryOpenNewModal()}
            >
                <img className="sidebar-item__new-img" src={plusIcon} alt="" />
            </button>
        </div>
    );
};

export default Sidebar;
