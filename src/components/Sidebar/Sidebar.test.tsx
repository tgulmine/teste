import React from 'react';
import { mount } from 'enzyme';
import { Sidebar } from '.';

describe('Sidebar', () => {
    it('renders without crashing', () => {
        const wrapper = mount(<Sidebar />);
        expect(wrapper.find(Sidebar)).toBeDefined();
    });
    it('does not render any pokemon', () => {
        const wrapper = mount(<Sidebar />);
        expect(wrapper.find('.sidebar-item__pokemon').length).toEqual(0);
    });
});
