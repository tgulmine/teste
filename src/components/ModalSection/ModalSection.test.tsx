import React from 'react';
import { mount } from 'enzyme';
import { ModalSection } from '.';

describe('ModalSection', () => {
    it('renders without crashing', () => {
        const wrapper = mount(<ModalSection title="title" />);
        expect(wrapper.find(ModalSection)).toBeDefined();
    });
});
