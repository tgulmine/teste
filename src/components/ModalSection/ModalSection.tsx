import React from 'react';
import './ModalSection.scss';

interface IModalSection {
    title: string;
}

const ModalSection = ({ title }: IModalSection) => {
    return (
        <div className="modalSection">
            <div className="modalSection__line" />
            <div className="modalSection__title typo typo--subtitle">
                {title}
            </div>
            <div className="modalSection__line" />
        </div>
    );
};

export default ModalSection;
