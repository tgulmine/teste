import React from 'react';
import { mount } from 'enzyme';
import { CreatePokemon } from '.';
import { ModalSection } from '../ModalSection';
import { PokemonListProvider } from '../../context/pokemonListContext';

describe('CreatePokemon', () => {
    it('renders without crashing', () => {
        const wrapper = mount(
            <PokemonListProvider>
                <CreatePokemon />
            </PokemonListProvider>
        );
        expect(wrapper.find(CreatePokemon)).toBeDefined();
    });
    it('renders correct number of sections', () => {
        const wrapper = mount(
            <PokemonListProvider>
                <CreatePokemon />
            </PokemonListProvider>
        );
        expect(wrapper.find(ModalSection).length).toEqual(3);
    });
});
