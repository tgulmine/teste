import React, { useState, useEffect } from 'react';
import './CreatePokemon.scss';
import { typesAPI } from '../../api';
import {
    usePokemonList,
    Pokemon,
    newPokemon,
} from '../../context/pokemonListContext';
import attackIcon from '../../assets/images/attackIcon.png';
import defenseIcon from '../../assets/images/defenseIcon.png';
import speedIcon from '../../assets/images/speedIcon.png';
import chevronDownIcon from '../../assets/images/chevronDownIcon.png';
import closeIcon from '../../assets/images/closeIcon.png';
import { InputInfo } from '../InputInfo';
import { ModalSection } from '../ModalSection';

const CreatePokemon = () => {
    const [name, setName] = useState<string>('');
    const [hp, setHp] = useState<number>(0);
    const [weight, setWeight] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const [types, setTypes] = useState<string[]>([]);
    const [ability1, setAbility1] = useState<string>('');
    const [ability2, setAbility2] = useState<string>('');
    const [ability3, setAbility3] = useState<string>('');
    const [ability4, setAbility4] = useState<string>('');
    const [attack, setAttack] = useState<number>(0);
    const [defense, setDefense] = useState<number>(0);
    const [specialAttack, setSpecialAttack] = useState<number>(0);
    const [specialDefense, setSpecialDefense] = useState<number>(0);
    const [speed, setSpeed] = useState<number>(0);
    const [pokemonTypes, setPokemonTypes] = useState<string[]>([]);

    const { setPokemonTemp } = usePokemonList();

    let pokemon: Pokemon = newPokemon;

    useEffect(() => {
        updatePokemon();
    }, [
        name,
        hp,
        weight,
        height,
        types,
        ability1,
        ability2,
        ability3,
        ability4,
        attack,
        defense,
        specialAttack,
        specialDefense,
        speed,
        updatePokemon,
    ]);

    // eslint-disable-next-line react-hooks/exhaustive-deps
    function updatePokemon() {
        pokemon.name = name;
        pokemon.hp = hp;
        pokemon.weight = weight;
        pokemon.height = height;
        pokemon.attack = attack;
        pokemon.defense = defense;
        pokemon.specialAttack = specialAttack;
        pokemon.specialDefense = specialDefense;
        pokemon.speed = speed;
        pokemon.types = types;
        const abilitiesTemp: string[] = [];
        if (ability1) abilitiesTemp.push(ability1);
        if (ability2) abilitiesTemp.push(ability2);
        if (ability3) abilitiesTemp.push(ability3);
        if (ability4) abilitiesTemp.push(ability4);
        pokemon.abilities = abilitiesTemp;
        setPokemonTemp(pokemon);
    }

    useEffect(() => {
        getPokemonTypes();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    async function getPokemonTypes() {
        try {
            const res = await typesAPI.get('');
            res.data.results.splice(18, 2);
            const types: string[] = [];
            res.data.results.forEach((result: any, index: number) => {
                types.push(result.name);
            });
            setPokemonTypes(types);
        } catch (err) {
            console.log(err);
        }
    }

    function selectType(type: string) {
        if (types.length < 2) {
            setTypes([...types, type]);
        }
    }

    function deleteType(type: string) {
        setTypes(types.filter(t => type !== t));
    }

    return (
        <div className="createPokemon">
            <InputInfo
                name="Nome"
                type="text"
                info={name}
                setInfo={setName}
                placeholder="Nome"
                icon=""
                tag=""
            />
            <InputInfo
                name="Hp"
                type="number"
                info={hp}
                setInfo={setHp}
                placeholder="HP"
                icon=""
                tag=""
            />
            <InputInfo
                name="Peso"
                type="number"
                info={weight}
                setInfo={setWeight}
                placeholder="Peso"
                icon=""
                tag="Kg"
            />
            <InputInfo
                name="Altura"
                type="number"
                info={height}
                setInfo={setHeight}
                placeholder="Altura"
                icon=""
                tag="Cm"
            />
            <ModalSection title="TIPOS" />
            <div className="createPokemon-dropdown">
                <div className="createPokemon-dropdown-info">
                    {types.map((type, index) => {
                        return (
                            <div
                                key={type + index}
                                className="createPokemon-dropdown-info__type"
                            >
                                <div className="createPokemon-dropdown-info__type-text typo typo--subtitle">
                                    {type}
                                </div>
                                <button
                                    type="button"
                                    className="createPokemon-dropdown-info__type-icon"
                                    onClick={() => deleteType(type)}
                                >
                                    <img src={closeIcon} alt="" />
                                </button>
                            </div>
                        );
                    })}
                    {types.length === 0 && (
                        <div className="createPokemon-dropdown-info__placeholder typo typo--subtitle">
                            Placeholder
                        </div>
                    )}

                    <img
                        className="createPokemon-dropdown-info__icon"
                        src={chevronDownIcon}
                        alt=""
                    />
                </div>
                <div className="createPokemon-dropdown-content">
                    {pokemonTypes &&
                        pokemonTypes.map((type, index) => (
                            <button
                                type="button"
                                key={index}
                                className="createPokemon-dropdown-element typo typo--subtitle"
                                onClick={() => selectType(type)}
                            >
                                {type}
                            </button>
                        ))}
                </div>
            </div>
            <ModalSection title="HABILIDADES" />
            <div className="createPokemon-abilities">
                <InputInfo
                    name=""
                    type="text"
                    info={ability1}
                    setInfo={setAbility1}
                    placeholder="Habilidade 1"
                    icon=""
                    tag=""
                />
                <InputInfo
                    name=""
                    type="text"
                    info={ability2}
                    setInfo={setAbility2}
                    placeholder="Habilidade 2"
                    icon=""
                    tag=""
                />
                <InputInfo
                    name=""
                    type="text"
                    info={ability3}
                    setInfo={setAbility3}
                    placeholder="Habilidade 3"
                    icon=""
                    tag=""
                />
                <InputInfo
                    name=""
                    type="text"
                    info={ability4}
                    setInfo={setAbility4}
                    placeholder="Habilidade 4"
                    icon=""
                    tag=""
                />
            </div>
            <ModalSection title="ESTATÍSTICAS" />
            <InputInfo
                name="Defesa"
                type="text"
                info={defense}
                setInfo={setDefense}
                placeholder="00"
                icon={defenseIcon}
                tag=""
            />
            <InputInfo
                name="Ataque"
                type="text"
                info={attack}
                setInfo={setAttack}
                placeholder="00"
                icon={attackIcon}
                tag=""
            />
            <InputInfo
                name="Defesa Especial"
                type="text"
                info={specialDefense}
                setInfo={setSpecialDefense}
                placeholder="00"
                icon={defenseIcon}
                tag=""
            />
            <InputInfo
                name="Ataque Especial"
                type="text"
                info={specialAttack}
                setInfo={setSpecialAttack}
                placeholder="00"
                icon={attackIcon}
                tag=""
            />
            <InputInfo
                name="Velocidade"
                type="text"
                info={speed}
                setInfo={setSpeed}
                placeholder="00"
                icon={speedIcon}
                tag=""
            />
            <div className="createPokemon-blank" />
        </div>
    );
};

export default CreatePokemon;
