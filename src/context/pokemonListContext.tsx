import * as React from 'react';

export interface Pokemon {
    sprite: string;
    name: string;
    height: number;
    weight: number;
    hp: number;
    types: string[];
    abilities: string[];
    attack: number;
    defense: number;
    specialAttack: number;
    specialDefense: number;
    speed: number;
}

export const newPokemon: Pokemon = {
    sprite: '',
    name: '',
    height: 0,
    weight: 0,
    hp: 0,
    types: [],
    abilities: [],
    attack: 0,
    defense: 0,
    specialAttack: 0,
    specialDefense: 0,
    speed: 0,
};

interface PokemonListContextType {
    pokemonList: Pokemon[];
    setPokemonList: Function;
    pokemonTemp: Pokemon;
    setPokemonTemp: Function;
    isPokemonListFull: Boolean;
    setIsPokemonListFull: Function;
    addPokemon: Function;
    releasePokemon: Function;
    editPokemon: Function;
}

const initialValues = {
    pokemonList: [],
    setPokemonList: () => {
        throw Error();
    },
    pokemonTemp: newPokemon,
    setPokemonTemp: () => {
        throw Error();
    },
    isPokemonListFull: false,
    setIsPokemonListFull: (pokemon: Pokemon) => {
        throw Error();
    },
    addPokemon: (pokemon: Pokemon) => {
        throw Error();
    },
    releasePokemon: (pokemon: Pokemon) => {
        throw Error();
    },
    editPokemon: (pokemon: Pokemon) => {
        throw Error();
    },
};

const PokemonListContext = React.createContext<PokemonListContextType>(
    initialValues
);

type Props = {
    children: React.ReactNode;
};

export const PokemonListProvider = ({ children }: Props) => {
    const [pokemonList, setPokemonList] = React.useState<Pokemon[]>([]);
    const [pokemonTemp, setPokemonTemp] = React.useState<Pokemon>(newPokemon);
    const [isPokemonListFull, setIsPokemonListFull] = React.useState<Boolean>(
        false
    );

    function addPokemon(pokemon: Pokemon) {
        const newPokemonList: Pokemon[] = pokemonList;
        newPokemonList.push(pokemon);
        setPokemonList(newPokemonList);
        if (pokemonList.length === 6) setIsPokemonListFull(true);
    }

    function releasePokemon(id: number) {
        const newPokemonList: Pokemon[] = pokemonList;
        newPokemonList.splice(id, 1);
        setPokemonList(newPokemonList);
        if (isPokemonListFull) setIsPokemonListFull(false);
    }

    function editPokemon(pokemon: Pokemon, id: number) {
        const newPokemonList: Pokemon[] = pokemonList;
        newPokemonList.splice(id, 1, pokemon);
        setPokemonList(newPokemonList);
    }

    return (
        <PokemonListContext.Provider
            value={{
                pokemonList,
                setPokemonList,
                pokemonTemp,
                setPokemonTemp,
                isPokemonListFull,
                setIsPokemonListFull,
                addPokemon,
                releasePokemon,
                editPokemon,
            }}
        >
            {children}
        </PokemonListContext.Provider>
    );
};

export const usePokemonList = () => React.useContext(PokemonListContext);
