import React, { useState, Dispatch, SetStateAction } from 'react';

export const MODAL_TYPES = {
    BASE: 'BASE',
    CAPTURE: 'CAPTURE',
    NEW: 'NEW',
};

interface MapContextType {
    isModalOpen: boolean;
    setModalOpen: Dispatch<SetStateAction<boolean>>;
    modalType: string;
    setModalType: Dispatch<SetStateAction<string>>;
    sidebarIndex: number;
    setSidebarIndex: Dispatch<SetStateAction<number>>;
    openBaseModal: Function;
    openCaptureModal: Function;
    openNewModal: Function;
}

const initialValues: MapContextType = {
    isModalOpen: false,
    setModalOpen: () => {
        throw Error();
    },
    modalType: MODAL_TYPES.CAPTURE,
    setModalType: () => {
        throw Error();
    },
    sidebarIndex: 6,
    setSidebarIndex: () => {
        throw Error();
    },
    openBaseModal: (id: number) => {
        throw Error();
    },
    openCaptureModal: () => {
        throw Error();
    },
    openNewModal: () => {
        throw Error();
    },
};

const MapContext = React.createContext<MapContextType>(initialValues);

interface Props {
    children: React.ReactNode;
}

export const MapProvider = ({ children }: Props) => {
    const [isModalOpen, setModalOpen] = useState(false);
    const [modalType, setModalType] = useState(MODAL_TYPES.CAPTURE);
    const [sidebarIndex, setSidebarIndex] = useState<number>(6);

    function openBaseModal(id: number) {
        setSidebarIndex(id);
        setModalType(MODAL_TYPES.BASE);
        setModalOpen(true);
    }
    function openCaptureModal() {
        setModalType(MODAL_TYPES.CAPTURE);
        setModalOpen(true);
    }
    function openNewModal() {
        setModalType(MODAL_TYPES.NEW);
        setModalOpen(true);
    }

    return (
        <MapContext.Provider
            value={{
                isModalOpen,
                setModalOpen,
                modalType,
                setModalType,
                openBaseModal,
                openCaptureModal,
                openNewModal,
                sidebarIndex,
                setSidebarIndex,
            }}
        >
            {children}
        </MapContext.Provider>
    );
};

export const useMapContext = () => React.useContext(MapContext);
