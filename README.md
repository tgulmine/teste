# PokeAPI Web

Esse projeto foi feito com base na [PokeAPI](https://pokeapi.co/), onde o usuário consegue receber dados de qualquer pokémon, capturá-los ou criar um próprio.

## Stack

### Desenvolvimento

-   UI: [React](https://reactjs.org/)
-   Controle de Estado: [ContextAPI](https://pt-br.reactjs.org/docs/context.html) e [React Hooks](https://pt-br.reactjs.org/docs/hooks-intro.html).
-   Estilos: [SASS](https://sass-lang.com/) com uso da nomenclatura [BEM](http://getbem.com/naming/)
-   Cliente HTTP: [Axios](https://github.com/axios/axios)
-   [Typescript](https://www.typescriptlang.org/)

### Testes

-   [Jest](https://jestjs.io/)
-   [Enzyme](https://airbnb.io/enzyme/)

### Linter

-   [ESlint](https://eslint.org/)
-   [Prettier](https://github.com/prettier/prettier)

## Uso

Instalar dependências:

```sh
npm install
```

Rodar o projeto:

```sh
npm start
```

## Estrutura do Projeto

-   `src/` código base;
-   `src/api` configurações da api;
-   `src/components` componentes isolados com seu estilo e index.tsx para facilitar imports e visualização no editor;
-   `src/pages` componentes com roteamento isolados com seu estilo;
-   `src/styles` uso da [Arquitetura 7-1](https://sass-guidelin.es/#the-7-1-pattern) para organizar o código do SASS.

## Comandos

```sh
# rodar a aplicação
npm start
# rodar testes
npm run test
```
